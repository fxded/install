
1. copy file into new arm (scp -r 10..:/home/install .)
2. run first_install
3. rename hosts hostname
4. vim /etc/default/grub -->> timer, quiet
5. update-grub
6. run docker_install_v2
7. copy .bashrc .conkyrc to ~/
8. copy config to ~/.ssh
9. install zsh(1,2 .p10.zsh.text for terminal, .p10.zsh2 for desktop)
10. weston.ini into .config/weston.ini(apt istall wayland-protocols weston) weston-launch
