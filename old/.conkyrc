--[[
Conky, a system monitor, based on torsmo

Any original torsmo code is licensed under the BSD license

All code written since the fork of torsmo is licensed under the GPL

Please see COPYING for details

Copyright (c) 2004, Hannu Saransaari and Lauri Hakkarainen
Copyright (c) 2005-2019 Brenden Matthews, Philip Kovacs, et. al. (see AUTHORS)
All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
]]

conky.config = {
    xinerama_head = 2,
    alignment = 'top_right',
    background = yes,
    border_width = 1,
    cpu_avg_samples = 2,
    default_color = 'white',
    default_outline_color = 'white',
    default_shade_color = 'white',
    double_buffer = true,
    draw_borders = false,
    draw_graph_borders = true,
    draw_outline = false,
    draw_shades = false,
    extra_newline = false,
    font = 'DejaVu Sans Mono:size=12',
    gap_x = 60,
    gap_y = 60,
    minimum_height = 300,
    minimum_width = 550,
    net_avg_samples = 2,
    no_buffers = true,
    out_to_console = false,
    out_to_ncurses = false,
    out_to_stderr = false,
    out_to_x = true,
    own_window = true,
    own_window_class = 'Conky',
    own_window_type = 'desktop',
    own_window_transparent = true,
    own_window_argb_visual = true,
    show_graph_range = false,
    show_graph_scale = false,
    stippled_borders = 0,
    update_interval = 1.5,
    use_spacer = 'none',
    use_xft = true,

    color0 = '#ffffff',
    color1 = '#999999',
    color2 = '#ff6600',
    color3 = 'green',
    color4 = '#000000',
    color5 = '#ffa300',
}

conky.text = [[
${color3}Info:$color3 $sysname $nodename $kernel $machine
$hr
${color yellow}1m: ${loadavg 1} 5m: ${loadavg 2} 15m: ${loadavg 3} $time desk:$desktop
${loadgraph 8 00ff00 ffffff -t -l}
${color grey}Uptime:$color $uptime ${color grey}ip address:$color ${addrs enp2s0}
${color grey}RAM Usage:$color $mem/$memmax - $memperc% ${membar 4}
${color grey}Swap Usage:$color $swap/$swapmax - $swapperc% ${swapbar 4}
${color grey}CPU Usage:$color $cpu% ${cpubar 4}
${color grey}Processes:$color $processes  ${color grey}Running:$color $running_processes ${color grey}Frequency (in MHz):$color $freq
$hr
${color grey}File systems:
 / $color${fs_used /}/${fs_size /} ${fs_bar 6 /}
${color5}nvme0n1::read:$color${diskio_read nvme0n1}${color5}      write:$color${diskio_write nvme0n1}
${color4}${diskiograph_read nvme0n1 30,275 efff00 3fc202}${diskiograph_write nvme0n1 30,275 6395f2 b202c2}
${color grey}Networking: Up:$color ${upspeed enp2s0} ${color grey}               Down:$color ${downspeed enp2s0}
${color4}${upspeedgraph enp2s0 30,275 efff00 3fc202}${downspeedgraph enp2s0 30,275 6395f2 b202c2}
${color1}${tcp_portmon 1025 65535 lservice 0}-${tcp_portmon 1025 65535 rip 0}-${tcp_portmon 1025 65535 rhost 0}
${color1}${tcp_portmon 1025 65535 lservice 1}-${tcp_portmon 1025 65535 rip 1}
${color1}${tcp_portmon 1025 65535 lservice 2}-${tcp_portmon 1025 65535 rip 2}
${color1}${tcp_portmon 1025 65535 lservice 3}-${tcp_portmon 1025 65535 rip 3}
${color1}${tcp_portmon 1025 65535 lservice 4}-${tcp_portmon 1025 65535 rip 4}
${color red}${tcp_portmon 1 1024 lservice 0}-${tcp_portmon 1 1024 rip 0}-${tcp_portmon 1 1024 rhost 0}
${color red}${tcp_portmon 1 1024 lservice 1}-${tcp_portmon 1 1024 rip 1}-${tcp_portmon 1 1024 rhost 1}
${color red}${tcp_portmon 1 1024 lservice 2}-${tcp_portmon 1 1024 rip 2}-${tcp_portmon 1 1024 rhost 2}
$hr
${color grey}Name              PID     CPU%   MEM%
${color lightgrey} ${top name 1} ${top pid 1} ${top cpu 1} ${top mem 1}
${color lightgrey} ${top name 2} ${top pid 2} ${top cpu 2} ${top mem 2}
${color lightgrey} ${top name 3} ${top pid 3} ${top cpu 3} ${top mem 3}
${color lightgrey} ${top name 4} ${top pid 4} ${top cpu 4} ${top mem 4}
]]
